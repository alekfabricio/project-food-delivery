import { Component, OnInit } from '@angular/core';

// Services
import { DataService } from '../services/data.service';

// Menu within food items
import menu from './menu.json';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  menu_list: any = menu.food;
  menu_list_selected: any[] = [];


  constructor(private data: DataService) {

  }

  ngOnInit() {
    this.data.share.subscribe(x => this.menu_list_selected = x);

    for (var element in this.menu_list) {
      this.menu_list[element].image = `assets/img/${this.menu_list[element].cuisine}.png`;
      this.menu_list[element].qntd = 0;
      this.menu_list[element].selected = false;
    }
  }

  // Qntd -> quantity of selected item
  addButtonClick(e, qntd) {
    var element = e;
    element.qntd = Number.parseInt(qntd.value);
    element.selected = true;
    this.menu_list_selected.push(element);

    this.menu_list.find(x => x.title == element.title).selected = true;
    
    this.data.updateData(this.menu_list_selected);
    this.data.updateAllData(this.menu_list);
  }

  removeButtonClick(element) {
    var item = this.menu_list_selected.find(x => x.title == element.title);
    
    if (item) {
      this.menu_list.find(x => x.title == element.title).selected = false;
      
      this.menu_list_selected.splice(this.menu_list_selected.indexOf(item), 1);
    }
    this.data.updateData(this.menu_list_selected);
    this.data.updateAllData(this.menu_list);
  }
}
