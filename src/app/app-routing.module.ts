import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Imported components
import { HomeComponent } from './home/home.component';
import { DialogComponent } from './dialog/dialog.component'; // for dev testing

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path:'dialog', component: DialogComponent},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
