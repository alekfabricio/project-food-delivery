import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private list_items = new BehaviorSubject<Array<any>>([]);
  private list_selected_items = new BehaviorSubject<Array<any>>([]);
  private total_sum = new BehaviorSubject<number>(0);

  public share = this.list_selected_items.asObservable();
  public shareAll = this.list_items.asObservable();
  public share_sum = this.total_sum.asObservable();

  constructor() { }

  updateAllData(items) {
    this.list_items.next(items);
  }

  updateData(selected_items) {
    this.list_selected_items.next(selected_items);

    var sum = 0;
    for (var element in selected_items) {
      sum += (Number.parseFloat(selected_items[element].price) *
        Number.parseFloat(selected_items[element].qntd));
    }
    this.total_sum.next(sum);
  }

}
