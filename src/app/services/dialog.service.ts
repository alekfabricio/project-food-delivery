import { Injectable } from '@angular/core';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})

export class DialogService {

  constructor(private dialog: MatDialog) { }

  openDialog(){
    this.dialog.open(DialogComponent, {
      width: '380px',
      height: '90%',
    });
  }

  
}
