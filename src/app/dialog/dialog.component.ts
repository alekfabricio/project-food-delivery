import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'modal-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

  menu_list: any[] = [];
  menu_list_selected: any[] = [];

  total_sum: String;

  constructor(private data: DataService) {
    
  }

  ngOnInit() {
    this.data.share.subscribe(x => this.menu_list_selected = x);
    this.data.shareAll.subscribe(y => this.menu_list = y);
    this.data.share_sum.subscribe(y => this.total_sum = y.toFixed(2));
  }

  removeButtonClick(element) {
    var item = this.menu_list_selected.find(x => x.title == element.title);
    if (item) {
      this.menu_list.find(x => x.title == element.title).selected = false;
      this.data.updateData(this.menu_list_selected);
      this.menu_list_selected.splice(this.menu_list_selected.indexOf(item), 1);
    }
    this.data.updateData(this.menu_list_selected); 
    this.data.updateAllData(this.menu_list);
  }

}
