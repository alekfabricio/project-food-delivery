import { Component, OnInit, } from '@angular/core';

// Services
import { DataService } from '../services/data.service';
import { DialogService } from '../services/dialog.service';

@Component({
  selector: 'fab-button',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.css']
})
export class FabComponent implements OnInit {

  menu_list_selected: any[] = [];
  total_sum: String;

  constructor(private data: DataService, private dialog: DialogService) {
    
  }

  ngOnInit() {
    this.data.share.subscribe(x => this.menu_list_selected = x);
    this.data.share_sum.subscribe(y => this.total_sum = y.toFixed(2));
  }

  showCart() {
    //console.log(this.menu_list_selected);
    this.dialog.openDialog();
  }
}
